<?php

namespace Andriyanto\ebriDwhService;

use Illuminate\Support\ServiceProvider;

class ebriDwhServiceServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/ebriDwhService.php' => config_path('ebriDwhServicephp'),
        ], 'config');

        $this->publishes([
            __DIR__.'/databases/migrations' => database_path('eBriDwhService.php'),
        ], 'migrations');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
